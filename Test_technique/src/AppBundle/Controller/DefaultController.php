<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }

    /**
     * @Rest\Get("api/students/{id}")
     */
    public function GetAllStudent($id)
    {
        $em = $this->getDoctrine()->getManager();

        $students = $em->getRepository('AppBundle:Student')->findAll();

        $result = [];

        foreach ($students as $student) 
        {
            if ($student->getDepartment()->getId() == $id)
                array_push($result, $student);
        }

        $serializer = $this->get('serializer');
        $json = $serializer->serialize(
            $result,
            'json'
        );

        return new Response($json);
    }
}
